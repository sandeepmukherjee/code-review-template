// Define data models for each tab
class Tab1DataModel: ObservableObject, Hashable {
    @Published var property1: String

    init(property1: String) {
        self.property1 = property1
    }

    // Implement Hashable protocol
    func hash(into hasher: inout Hasher) {
        hasher.combine(ObjectIdentifier(self).hashValue)
    }

    static func == (lhs: Tab1DataModel, rhs: Tab1DataModel) -> Bool {
        return lhs === rhs
    }
}

class Tab2DataModel: ObservableObject, Hashable {
    @Published var property1: Bool
    @Published var property2: Int

    init(property1: Bool, property2: Int) {
        self.property1 = property1
        self.property2 = property2
    }

    // Implement Hashable protocol
    func hash(into hasher: inout Hasher) {
        hasher.combine(ObjectIdentifier(self).hashValue)
    }

    static func == (lhs: Tab2DataModel, rhs: Tab2DataModel) -> Bool {
        return lhs === rhs
    }
}
//. Main VC Code Here

import UIKit
import Combine


class TabViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!

    var dataSource: UITableViewDiffableDataSource<Int, Any>!
    var cancellables: Set<AnyCancellable> = []
    var currentData: [[Any]] = [] // 2D array to store data models for each tab

    override func viewDidLoad() {
        super.viewDidLoad()

        // Register different cell types for each tab
        tableView.register(Tab1Cell.self, forCellReuseIdentifier: "Tab1Cell")
        tableView.register(Tab2Cell.self, forCellReuseIdentifier: "Tab2Cell")

        // Initialize data for each tab
        let tab1Data = [Tab1DataModel(), Tab1DataModel()]
        let tab2Data = [Tab2DataModel(), Tab2DataModel(), Tab2DataModel()]
        // Add data for other tabs here

        currentData = [tab1Data, tab2Data]

        // Create the data source
        dataSource = UITableViewDiffableDataSource(tableView: tableView) { tableView, indexPath, item in
            // Dequeue the correct cell based on the data model for the current tab
            if let tab1Data = item as? Tab1DataModel {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Tab1Cell", for: indexPath) as! Tab1Cell
                cell.configure(with: tab1Data)
                return cell
            } else if let tab2Data = item as? Tab2DataModel {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Tab2Cell", for: indexPath) as! Tab2Cell
                cell.configure(with: tab2Data)
                return cell
            }
            // Return a default cell if the data model is of an unknown type
            return UITableViewCell()
        }

        // Update the initial data for the first tab
        updateTableViewData(tabIndex: segmentedControl.selectedSegmentIndex)

        // Setup segmented control action
        segmentedControl.addTarget(self, action: #selector(segmentedControlDidChangeValue(_:)), for: .valueChanged)

        // Observe changes in the data models using Combine
        currentData.flatMap { $0.compactMap { $0 as? ObservableObject } }
            .publisher
            .sink { [weak self] _ in
                // Reload the table view when any property changes in the data models
                self?.updateTableViewData(tabIndex: self?.segmentedControl.selectedSegmentIndex ?? 0)
            }
            .store(in: &cancellables)

        // Set the table view delegate
        tableView.delegate = self
    }

    @objc func segmentedControlDidChangeValue(_ sender: UISegmentedControl) {
        // Update the data for the selected tab
        updateTableViewData(tabIndex: sender.selectedSegmentIndex)
    }

    func updateTableViewData(tabIndex: Int) {
        let dataForTab = currentData[tabIndex]

        var snapshot = NSDiffableDataSourceSnapshot<Int, Any>()
        snapshot.appendSections([0])
        snapshot.appendItems(dataForTab, toSection: 0)

        dataSource.apply(snapshot, animatingDifferences: true)
    }
}

// Implement UITableViewDelegate extension
extension TabViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Get the data model for the selected tab and row
        let tabData = currentData[segmentedControl.selectedSegmentIndex][indexPath.row]

        // Update the data model property based on the selected tab
        if let tab1Data = tabData as? Tab1DataModel {
            tab1Data.property1 = "Updated Property1"
            // You can update other properties as needed
        } else if let tab2Data = tabData as? Tab2DataModel {
            tab2Data.property1.toggle()
            // You can update other properties as needed
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // You can calculate and return the height for the cell here
        return 44.0 // For example, return a fixed height of 44.0 points
    }
}
