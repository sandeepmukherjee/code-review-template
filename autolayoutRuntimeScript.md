# Xiblint Pre-Commit Hook

This is a pre-commit hook that checks XIB and storyboard files for Auto Layout constraint errors, using [xiblint](https://github.com/lyft/xiblint).

## Installation

1. Install `xiblint` using pip: `pip install xiblint`
2. Create a file named `pre-commit` in the `.git/hooks` directory of your project, if it doesn't already exist. If it does exist, open it in a text editor.
3. Add the following code to the file:

```bash
#!/bin/bash

IFS=$'\n'
FILES=($(git diff --name-only --diff-filter=AM --cached HEAD | grep -E '\.(storyboard|xib)$'))
FILES+=($(git diff --name-only --diff-filter=AM HEAD | grep -E '\.(storyboard|xib)$'))

if [ ${#FILES[@]} -gt 0 ]; then
  xiblint "${FILES[@]}"
  if [ $? -ne 0 ]; then
    echo "XIB/Storyboard constraint errors detected. Aborting commit."
    exit 1
  fi
fi

exit 0
```

4. Save and close the file.
5. Make the file executable: `chmod +x .git/hooks/pre-commit`

## Usage

The pre-commit hook will run automatically when we try to make a commit that includes changes to XIB or storyboard files.

If any Auto Layout constraint errors are detected by `xiblint`, the commit will be aborted and an error message will be displayed. we will need to fix the errors and try again.

## Xcode Build Script

We can also add an Xcode build script to check for constraint errors on every build. Here's an example script:

```bash
#!/bin/bash

# Find all XIB and storyboard files that have been modified in the current commit or are newly added (but not yet committed)
IFS=$'\n'
FILES=($(git diff --name-only --diff-filter=AM --cached HEAD | grep -E '\.(storyboard|xib)$'))
FILES+=($(git diff --name-only --diff-filter=AM HEAD | grep -E '\.(storyboard|xib)$'))

if [ ${#FILES[@]} -gt 0 ]; then
  xiblint "${FILES[@]}"
  if [ $? -ne 0 ]; then
    echo "XIB/Storyboard constraint errors detected. Aborting build."
    exit 1
  fi
fi

# Touch a dummy file to indicate that the script has completed
touch "${SRCROOT}/output_dependency.txt"
exit 0
```

1. Create a new "Run Script" phase in your Xcode project.
2. Copy the script above into the script text box.
3. Make sure the script is set to run before the "Compile Sources" phase.
4. Save and close the Xcode project. 

The Xcode build script will now run automatically on every build, and will check for constraint errors in any XIB or storyboard files that have been modified since the last commit. If any errors are detected, the build will be aborted and an error message will be displayed.

![SCREENSHOT](AutolayoutError.png)

## Limitations

The basic pre-commit hook we created above checks all XIB and storyboard files in the project. However, this may not always be desirable as some files may not need to be checked. We can modify the code to exclude specific files or directories that do not need to be checked.

## Customization

We can customize the Xiblint configuration by creating a `.xiblint.json` file in the root directory of your Xcode project. The `.xiblint.json` file should contain a JSON object with the configuration options for Xiblint. For example, the following configuration enables all Xiblint rules:
```
{
  "rules": [
    "accessibility_format",
    "autolayout_frames"
  ],
  "paths": {
    "Pods": {
      "rules": []
    },
    "InaccessibleFeature": {
      "excluded_rules": [
        "accessibility_*"
      ]
    },
    "Modules": {
      "rules": ["some_rule"],
      "rules_config": {
        "some_rule": {
          "some_rule_specific_option": true
        }
      }
    }
  }
}
```
